function subpathsToSketchRNN(subpaths, minPoints, maxPoints) {
    // simplify / reduce inefficient paths
    var subpaths = optimiseSubpaths(subpaths);
    // process paths to result to points within desired range ARGS (subpaths, subdivideInterval, flattenFactor, simplifyFactor, minPoints, maxPoints, count)
    subpaths = processSubpaths(subpaths, null, 1, 0.2, minPoints, maxPoints, 0);
    return toSketchRNNData(subpaths);
}
function optimiseSubpaths(subpaths) {
    var tempSubpaths = subpaths.slice();
    tempSubpaths.map(function(subpath) {
        subpath.reduce();
        return subpath;
    });
    return tempSubpaths;
};
function simplifySubpaths(subpaths, simplifyFactor) {
    var tempSubpaths = subpaths.slice();
    var longestLength = longestSubpathLength(subpaths);
    tempSubpaths.map(function(subpath) {
        if (subpath.segments.length > 1) {
            var lengthFactor = subpath.length/longestLength;
            subpath.simplify(simplifyFactor);
        }
        return subpath;
    });
    return tempSubpaths;
};
function flattenSubpaths(subpaths, flattenFactor) {
    var tempSubpaths = subpaths.slice();
    var longestLength = longestSubpathLength(subpaths);
    tempSubpaths.map(function(subpath) {
        if (subpath.segments.length > 1) {
            var lengthFactor = subpath.length/longestLength;
            subpath.flatten(flattenFactor * lengthFactor);
        }
        return subpath;
    });
    return tempSubpaths;
};
function subdivideSubpaths(subpaths, subdivideInterval) {
    var tempSubpaths = subpaths.slice();
    tempSubpaths.map(function(subpath, i) {
        var indexesOfFlatCurves = [];
        subpath.curves.forEach(function(c, i) {
            if (!c.hasHandles() && c.hasLength()) {
                indexesOfFlatCurves.push(i);
            }
        });
        var flatCurves = [];
        subpath.segments.forEach(function(s, i) {
            if (indexesOfFlatCurves.indexOf(i) >= 0) {
                flatCurves.push(s.curve);
            }
        });
        flatCurves.forEach(function(curve, i) {
            for (var pos = curve.length-subdivideInterval; pos > 0; pos-= subdivideInterval) {
                curve.divideAt(pos);
            }
        });
        return subpath;
    })
    return tempSubpaths;
}
function pointsInSubpaths(subpaths) {
    var totalSegments = 0;
    subpaths.forEach(function(subpath) {
        totalSegments += 2;
        subpath.segments.forEach(function() {
            totalSegments += 1;
        });
    });
    return totalSegments;
};
function lengthOfSubpaths(subpaths) {
    var totalPathLength = 0;
    subpaths.forEach(function(path) { // calculation
        totalPathLength += lengthOfSubpath(path);
    });
    return totalPathLength;
}
function longestSubpathLength(subpaths) {
    var length = 0;
    subpaths.forEach(function(path) { // calculation
        if (lengthOfSubpath(path > length)) {
            length = lengthOfSubpath(path);
        }
    });
    return length;
}
function lengthOfSubpath(subpath) {
    return subpath.length;
}
function processSubpaths(subpaths, subdivideInterval, flattenFactor, simplifyFactor, minPoints, maxPoints, count) {
    var tempSubpaths = flattenSubpaths(subpaths, null); // flatten curves by default amount
    var numPoints = pointsInSubpaths(tempSubpaths);
    var maxFlattenFactor = 12;
    var minSubdivideInterval = 1;
    var maxSimplifyFactor = 1;
    console.log(count, numPoints, 'simplifyFactor: ', simplifyFactor, 'subdivideInterval: ' + subdivideInterval, 'flattenFactor: ' + flattenFactor);

    if (simplifyFactor >= maxSimplifyFactor) { // break if maximum simplification factor is reached
        return tempSubpaths;
    } else if (numPoints < minPoints) { // if there are not enough points
        if (!subdivideInterval) {
            subdivideInterval = lengthOfSubpaths(subpaths)/60;
        }
        tempSubpaths = subdivideSubpaths(subpaths, subdivideInterval);
        tempSubpaths = flattenSubpaths(tempSubpaths, null); // flatten
        if (subdivideInterval > minSubdivideInterval) {
            subdivideInterval--;
        }
        count += 1;
        return processSubpaths(subpaths, subdivideInterval, flattenFactor, simplifyFactor, minPoints, maxPoints, count)
    } else if (numPoints > maxPoints) { // if there are too many points
        tempSubpaths = simplifySubpaths(subpaths, simplifyFactor);
        tempSubpaths = flattenSubpaths(tempSubpaths, flattenFactor);
        tempSubpaths = optimiseSubpaths(tempSubpaths);

        if (flattenFactor < maxFlattenFactor) {
            flattenFactor = parseFloat((flattenFactor + 0.1).toFixed(2));
        } else if (flattenFactor >= maxFlattenFactor) { // max flattenFactor before simplifying further
            flattenFactor = 0.25;
            simplifyFactor = parseFloat((simplifyFactor + 0.1).toFixed(2));
        }
        count += 1;
        return processSubpaths(subpaths, subdivideInterval, flattenFactor, simplifyFactor, minPoints, maxPoints, count)
    } else { // if num points are within the range, return paths
        return tempSubpaths;
    }
}
function toSketchRNNData(subpaths) {
    // console.log(subpaths)
    // Sketch Data Structure
    // var sketch = [[ x, y, penUp],...]
    var firstPoint = subpaths[0].segments[0].point;
    var offsetX = -firstPoint.x;
    var offsetY = -firstPoint.y;

    var startX = Number((-offsetX).toFixed(2));
    var startY = Number((-offsetY).toFixed(2));
    var sketch = [[startX, startY, 0]];

    subpaths.forEach(function(subpath, subpathIndex) {
        var subPathSegments = subpath.segments.slice();
        subpath.segments.forEach(function(segment, segmentIndex) {
            if (!(subpathIndex == 0 && segmentIndex == 0)) {
                var currentX = segment.point.x;
                var currentY = segment.point.y;
                var previousX;
                var previousY;

                var prevSubpath = subpaths[subpathIndex - 1];
                if (segmentIndex == 0 && prevSubpath.segments.length) {
                    var prevSubpathSegments = prevSubpath.segments;
                    var lastPointOfPrevSubpath = prevSubpathSegments[prevSubpathSegments.length - 1].point;
                    var offSetSketchPoint = sketch[sketch.length -2];

                    previousX = lastPointOfPrevSubpath.x + offSetSketchPoint[0];
                    previousY = lastPointOfPrevSubpath.y + offSetSketchPoint[1];
                } else if (subPathSegments.length && segmentIndex > 0) {
                    previousX = subPathSegments[segmentIndex - 1].point.x;
                    previousY = subPathSegments[segmentIndex - 1].point.y;
                }

                var newX = Number((currentX - previousX).toFixed(2));
                var newY = Number((currentY - previousY).toFixed(2));
                var sketchPoint = [newX, newY, 0];
                sketch.push(sketchPoint);
                if (segmentIndex == subpath.segments.length-1) {
                    var firstPoint = subPathSegments[0].point;
                    var finalX = Number((firstPoint.x - currentX).toFixed(2));
                    var finalY = Number((firstPoint.y - currentY).toFixed(2));
                    sketch.push([finalX, finalY, 0]); // close loop
                    sketch.push([0, 0, 1]); // lift pen up
                }
            }
        });
    });
    return sketch;
}

module.exports = {
    subpathsToSketchRNN: subpathsToSketchRNN
}
