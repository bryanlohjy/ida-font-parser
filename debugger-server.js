// serve ui page
var express =  require('express');
var process = require('process');
var dataType = process.argv[2];
var app = express();
var server = app.listen(7000); // create server

app.use('/static', express.static('data'));
app.use('/static', express.static('debugger', { // serve files in the ui folder
	index : 'index.html'
}));

console.log('Server running at http://localhost:7000/static');

var open = require("open");
open('http://localhost:7000/static?data=' + dataType);
