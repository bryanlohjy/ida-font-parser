var jsdom = require('jsdom/lib/old-api.js').jsdom;
var opentype = require('opentype.js');
var paper = require('paper-jsdom');
var fs = require('fs');
var sketchUtils = require('./sketch-rnn-data-utils.js');
var document = jsdom('<canvas id="typeCanvas"></canvas>'); // creating DOM
var window = document.defaultView;

var canvas = document.getElementById('typeCanvas'); // setting up paper canvas
canvas.width = 256;
canvas.height = 256;
paper.setup(canvas);

var resultLog = {
    glyphsOutOfPointRange: 0,
    glyphsOutOfBounds: [],
    fontsFailed: [],
    fontsUnsupported: [],
    singleOutline: [],
    emptyGlyph: [],
    undefinedUnicode: [],
    rescaledGlyphs: [],
    invalidBoundingBox: [],
    failedToLoad: []
};

parseFontDirectory('F:/font-dataset-full/');
// Parse each font in directory
function parseFontDirectory (baseDir) {
    fs.readdir(baseDir, function(err, items) {
        var data = {};
        // All Data in One JSON ======================================
        for (var i=0; i < 100 ; i++) {
            var path = baseDir + items[i];
            var fontData = parseFont(path, 'A', i);
            if (fontData && fontData.length > 0) {
                data[items[i]] = fontData;
            }
            console.log(i, items[i].replace(/^.*[\\\/]/, ''));
        }
        fs.writeFile('data/data.json', JSON.stringify(data), function(err) { // write data
            if (err) {
                return console.log(err);
            }
        });
        // As individual files ======================================
        // for (var i=0; i < items.length ; i++) {
        //     var path = 'F:/font-dataset/' + items[i];
        //     var fontData = parseFont(path, 'A');
        //     if (fontData && fontData.length > 0) {
        //         data.A = fontData;
        //         console.log(items[i]);
        //         var filename = items[i].replace(/^.*[\\\/]/, '');
        //         fs.writeFile('data/individual-json/'+ filename +'.json', JSON.stringify(data), function(err) { // write data
        //             if (err) {
        //                 return console.log(err);
        //             }
        //         });
        //     }
        // }
        console.log('The files were saved!');
        console.log('===================');
        console.log('These failed to load: ', resultLog.failedToLoad);
        console.log('No glyph found for: ', resultLog.emptyGlyph);
        console.log('No unicode character found for: ', resultLog.undefinedUnicode);
        console.log('Invalid bounding box for: ', resultLog.invalidBoundingBox);
        console.log('Glyphs have been rescaled: ', resultLog.rescaledGlyphs);



        console.log('Glyphs exceeding point range', resultLog.glyphsOutOfPointRange);
        console.log('Glyphs out of bounds', resultLog.glyphsOutOfBounds);
        console.log('Fonts failed', resultLog.fontsFailed);
        console.log('These fonts may be unsupported', resultLog.fontsUnsupported);
        console.log('These fonts may be out of bounds', resultLog.singleOutline);
    });
}

function parseFont(ttf, charString) {
    var chars = charString;
    var temp = {};
    chars.split('').map(function(glyph) {
        var pathData = parseGlyph(ttf, glyph, charString);
        // if (pathData) {
        //     if (pathData.length < 60) {
        //         resultLog.glyphsOutOfPointRange += 1;
        //         // console.log('SKETCHPOINTS BELOW 60 at', pathData.length);
        //     } else if (pathData.length > 92) {
        //         resultLog.glyphsOutOfPointRange += 1;
        //         // console.log('SKETCHPOINTS EXCEEDING 92 at', pathData.length);
        //     } else {
        //         temp[glyph] = pathData;
        //     }
        // }
        if (pathData) {
            temp[glyph] = pathData;
        }
    });
    if (Object.keys(temp).length) {
        return JSON.stringify(temp);
    } else {
        return null;
    }
}

function parseGlyph(ttf, glyph, charString) {
    var svgGlyph = ttfToSVG(ttf, glyph);
    paper.project.clear();

    if (svgGlyph) {
        var compoundPath = paper.project.importSVG(svgGlyph);
        if (!compoundPath.children) { // construct compound path if path is single
            compoundPath = new paper.CompoundPath(compoundPath);
        }
        // Positioning glyph
        var baselineX = 5;
        var baselineY = canvas.height - 5;
        compoundPath.bounds.left = baselineX;
        compoundPath.bounds.bottom = baselineY;
        var rightBound = compoundPath.bounds.right;
        var topBound = compoundPath.bounds.top;
        // resize if glyph exceeds right and top boundaries
        if (rightBound >= canvas.width || topBound <= 0) {
            var scaleFactor;
            if (rightBound > topBound) { // check if too tall / too wide
                scaleFactor = (canvas.width - 5) / rightBound;
                compoundPath.scale(scaleFactor, new paper.Point(baselineX, baselineY));
            } else {
                scaleFactor = baselineY / (baselineY + Math.abs(rightBound));
                compoundPath.scale(scaleFactor, new paper.Point(baselineX, baselineY));
            }
            resultLog.rescaledGlyphs.push(ttf + ': ' + glyph);
        }
        // sketch rnn data ======
        // subdivide path
        return processCompoundPath(compoundPath, ttf);

        // svg path data ======
        // return compoundPath.pathData
    } else {
        return null;
    }
}

function ttfToSVG(ttf, glyph) { // ttf -> svg path data
    try {
        var font = opentype.loadSync(ttf);
    } catch (err) {
        resultLog.failedToLoad.push(ttf + ': ' + glyph);
        return null;
    }

    var fontPath = font.getPath(glyph, 10, 245, 200, {
            features: false
        });

    if (fontPath) {
        var validGlyph = true;
        // check for valid unicode
        if (!font.charToGlyph('glyph').unicode) {
            validGlyph = false;
            resultLog.undefinedUnicode.push(ttf + ': ' + glyph);
        }
        // check for glyph path
        if (fontPath.commands.length == 0) {
            validGlyph = false;
            resultLog.emptyGlyph.push(ttf + ': ' + glyph);
        }
        // check for blank characters with paths
        var bounds = fontPath.getBoundingBox();
        if (bounds.x1 === bounds.x2 || bounds.y1 === bounds.y2) {
            validGlyph = false;
            resultLog.invalidBoundingBox.push(ttf + ': ' + glyph);
        }

        if (validGlyph) {
            return fontPath.toSVG();
        } else {
            return null;
        }
    } else {
        return null;
    }
}
//================================================================
function processCompoundPath(compoundPath, ttf) {
    var subpaths = getSubpaths(compoundPath);
    var subpathsAreOrthogonal = areAllSubpathsOrthogonal(subpaths);
    if (subpathsAreOrthogonal) {
        // log if all curves are horizontal / vertical - could be unsupported
        resultLog.fontsUnsupported.push(ttf);
    }
    return sketchUtils.subpathToSketchRNN(subpaths, 60, 92);
}

function areAllSubpathsOrthogonal(subpaths) {
    var isOrthogonal = true;
    subpaths.forEach(function(path) {
        path.curves.forEach(function(c, i) {
            if (!(c.isHorizontal() || c.isVertical())) {
                isOrthogonal = false;
            }
        });
    });
    return isOrthogonal;
}

function getSubpaths(path) { // Get individual paths from compound path
    var subPaths = [];
    if (path.hasChildren()) {
        path.children.forEach(function(path, i) {
            subPaths.push(path);
        });
    } else {
        subPaths.push(path);
    }
    return subPaths;
}

//================================================================
