// initialise canvas
var canvas = document.getElementById('canvas');
canvas.width = 255;
canvas.height = 255;
var ctx = canvas.getContext('2d');
var dataType = getUrlParameter('data');
//  load sketch data
var Debugger = {
  	sketches: [],
  	loadData: function(ctx, dataType, view, opts) {
        var dataFile = (dataType == 'svg') ? 'data-svg.json' : 'data.json';
  		readTextFile(dataFile, function(text) {
  			var data;
  			data = JSON.parse(text);
  			Object.keys(data).forEach(function(font) {
  				data[font] = JSON.parse(data[font]);
  				Debugger.sketches.push(data[font].A);
  			});
  			initUI(data, dataType);
  			switch (view) {
  				case 'glyph':
  					Debugger.drawGlyphView(ctx, dataType, Debugger.sketches, opts);
  					break;
  				case 'grid':
  					Debugger.drawGridView(ctx, dataType, Debugger.sketches, opts);
  			}
  		});
  	},
	drawSketchRNNGlyph: function(ctx, data) {
		// starting drawing params
        var penDown = false;
		var curX = 0;
		var curY = 0;
		if (data) { // draw glyph
			ctx.beginPath();
			for (var i in data) {
				var curPoint = data[i];
				var nextX = curX + curPoint[0];
				var nextY = curY + curPoint[1];
				if (penDown) {
					// draw point
					ctx.beginPath();
					ctx.arc(nextX, nextY, 1, 0, 2 * Math.PI, false);
					ctx.fillStyle = 'blue';
					ctx.fill();
					// draw stroke
					ctx.lineWidth=1;
					ctx.strokeStyle='blue'; // Green path
					ctx.moveTo(curX, curY);
					ctx.lineTo(nextX, nextY);
                    ctx.stroke();
				}
				curX = nextX;
				curY = nextY;
				penDown = (curPoint[2] == 0);
			}
			// indicate origin with ellipse
			var origin = data[0];
			ctx.beginPath();
			ctx.arc(origin[0], origin[1], 3, 0, 2 * Math.PI, false);
			ctx.fillStyle = 'blue';
			ctx.fill();
		}
	},
    drawSVGGlyph: function(ctx, data) {
        if (data) {
            var path = new Path2D(data.toString());
            ctx.fillStyle = "black";
            ctx.fill(path);
        }
    },
	drawGlyphView: function(ctx, dataType, data, opts) {
		var glyphIndex = (opts && opts.glyphIndex) ? opts.glyphIndex : 0;
		updateUI('glyph', { glyphData: data[glyphIndex] });
        if (dataType == 'svg') {
            Debugger.drawSVGGlyph(ctx, data[glyphIndex]);
        } else {
		    Debugger.drawSketchRNNGlyph(ctx, data[glyphIndex]);
        }
	},
	drawGridView: function(ctx, dataType, data, opts) {
		updateUI('grid', { gridData: data });
		var maxColumns = (opts && opts.columns) ? opts.columns : 10;
		var maxRows = (data.length % maxColumns == 0) ? data.length / maxColumns : parseInt(data.length / maxColumns) + 1;
		var scale = (opts && opts.scale) ? opts.scale : 0.2;
		var curRow = -1; // temp as -1, will be set to zero on first iteration
		canvas.width = 255 * maxColumns * scale;
		canvas.height = 255 * maxRows * scale;
		for (var i in data) {
			var col = i % maxColumns;
			if (col == 0) { // new row
				curRow++;
			}
			ctx.setTransform(1, 0, 0, 1, 0, 0);
			ctx.scale(scale, scale);
			ctx.translate(col * 255, curRow * 255);
			// draw rect bounds
			if (opts.indicateEnds != false && (col == maxColumns - 1 || curRow == maxRows-1)) {
                ctx.strokeStyle = 'red';
			} else {
                ctx.strokeStyle = 'lightgrey';

            }
            ctx.lineWidth = 2;
            ctx.strokeRect(0, 0, 255, 255);
			// draw glyph
            if (dataType == 'svg') {
                Debugger.drawSVGGlyph(ctx, data[i]);
            } else {
			    Debugger.drawSketchRNNGlyph(ctx, data[i]);
            }
		}
	},
	render: function(ctx, dataType, view, opts) {
		if (view == 'glyph') {
			ctx.setTransform(1, 0, 0, 1, 0, 0);
			canvas.width = 255;
			canvas.height = 255;
		}
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		if (this.sketches.length <= 0) {
			this.loadData(ctx, dataType, view, opts);
		} else {
			switch (view) {
				case 'glyph':
					Debugger.drawGlyphView(ctx, dataType, Debugger.sketches, opts);
					break;
				case 'grid':
					Debugger.drawGridView(ctx, dataType, Debugger.sketches, opts);
			}
		}
	}
};

function updateUI(view, data) {
	// update checkbox
	var gridCheckbox = document.getElementById('grid-checkbox');
	if (view == 'glyph' && gridCheckbox.checked) {
		gridCheckbox.checked = false;
	} else if (view == 'grid' && !gridCheckbox.checked) {
		gridCheckbox.checked = true;
	}
	var debugInfo = document.getElementById('debug-info');
	debugInfo.innerHTML = '';
	if (view == 'glyph') {
		if (data.glyphData) {
			debugInfo.innerHTML = 'Number of Points: ' + data.glyphData.length;
		}
	} else if (view == 'grid') {
		if (data.gridData) {
			debugInfo.innerHTML = 'Number of Glyphs: ' + data.gridData.length;
		}
	}
}

function initUI(sketchData, dataType) {
	// Initialise font dropdown ==========================
	var fontDropdown = document.getElementById('font-dropdown');

	// Populating dropdown with json options
	Object.keys(sketchData).forEach(function(key, index) {
    	var option = document.createElement('option');
    	var label = document.createTextNode(key.toString());
    	option.value = key;
    	option.text = key;
    	fontDropdown.add(option);
    });

    fontDropdown.addEventListener('change', function(e) {
		Debugger.render(ctx, dataType, 'glyph', {
			glyphIndex : e.target.selectedIndex
		});
    });

    // Initialise char dropdown ==========================
	var glyphDropdown = document.getElementById('glyph-dropdown');

	// Populating dropdown with json options
	Object.keys(sketchData[fontDropdown.value]).forEach(function(key, index) {
    	var option = document.createElement('option');
    	var label = document.createTextNode(key.toString());
    	option.value = key;
    	option.text = key;
    	glyphDropdown.add(option);
    });

    // Initialise char dropdown ==========================
    var gridCheckbox = document.getElementById('grid-checkbox');
    gridCheckbox.checked = true;
    gridCheckbox.addEventListener('change', function(e) {
    	if (e.target.checked) {
			Debugger.render(ctx, dataType, 'grid', {
				columns : 60,
				scale : 0.2,
				indicateEnds: true
			});
    	} else {
			Debugger.render(ctx, dataType, 'glyph', {
				glyphIndex : fontDropdown.selectedIndex
			});
    	}
    });
}

Debugger.render(ctx, dataType, 'grid', {
	columns : 60,
	scale : 0.2,
	indicateEnds: true
});

// Debugger.render(ctx, 'glyph', {
// 	glyphIndex: 2
// });
